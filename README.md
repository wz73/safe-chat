# Safe Chat - A CS50 Project

<img src="./public/assets/logo.png" style="margin: 1em;" width="120" height="120">

Harvard's CS50 Introduction to Computer Science

David J. Malan malan@harvard.edu<br>
Brian Yu brian@cs.harvard.edu

Video [Demo](https://youtube.com/?v=)

## About

Safe chat is a fast, secure, and user-friendly instant messaging app. The platform allows anyone to send end-to-end encrypted messages.<br>

User can log in and send self destructing messages to anyone. You may create an account which is saved into SQLite3 database for quick accessibility, which password is hashed using bcrypt hashing algorithm.<br>

## Installation

Download required dependencies
```bash
cd public
npm install
cd ../server
npm install
cd ..
```

## Usage

1. Deploy public folder with http-server
- with secure protocol
    ```bash
    cd public
    npm run startHttps
    cd ..
    ```
- without ssl certificate
    ```bash
    cd public
    npm run start
    cd ..
    ```
2. Create users database
```bash
cd database
cat users.sql | sqlite3 users.db
cd ..
```
3. Initialize server
```bash
cd server
npm run start
```

## Api

Server side api routes

### /api

Check wether server api is working correctly.

Method: `'GET, PUT, POST, DELETE`<br>
Status Code: `200`<br>
Status Message: `{"response": "Api working"}`<br>
Response type: `string`

### /api/user/login

Fetch an existing user token

Method: `POST`<br>
Response expected (JSON):
```json
{
    "username": $username,
    "password": $password
}
```
- Username must match with password

Error Status Code: `401`<br>
Success Status Code: `200`<br>
Status Message: `{"response": $token}`<br>
Response type: `string`

### /api/user/create

Creates a new user

Method: `POST`<br>
Response expected (JSON):
```json
{
    "username": $username,
    "password": $password
}
```
- Username can't be repeated in database
- Username must have a length between 3-20 characters
- Password must have a minimum length of 8 characters
- Password must contain lowercases, uppercases, and digits

Error Status Code: `400`<br>
Success Status Code: `200`<br>
Status Message: `{"response": $token}`<br>
Response type: `string`

### /api/user/delete

Deletes an existing user

Method: `POST`<br>
Response expected (JSON):
```json
{
    "username": $username,
    "token": $token
}
```
- Username must match with token

Error Status Code: `400, 401`<br>
Success Status Code: `200`<br>
Status Message: `{"response": "User deleted successfully"}`<br>
Response type: `string`

### /api/user/contacts

Retrieves user's contact list

Method: `POST`<br>
Response expected (JSON):
```json
{
    "username": $username,
    "token": $token
}
```
- Username must match with token

Error Status Code: `400, 401`<br>
Success Status Code: `200`<br>
Status Message: `{"response": $contacts}`<br>
Response type: `array`

### /api/user/contacts/create

Creates a new contact

Method: `POST`<br>
Response expected (JSON):
```json
{
    "username": $username,
    "token": $token,
    "contact": $contact
}
```
- Username must match with token
- Contact can't be user itself
- Contact can't already be in the contact list

Error Status Code: `400, 401`<br>
Success Status Code: `200`<br>
Status Message: `{"response": "Contact was created"}`<br>
Response type: `string`

### /api/user/contacts/delete

Deletes an user from the contact list

Method: `POST`<br>
Response expected (JSON):
```json
{
    "username": $username,
    "token": $token,
    "contact": $contact
}
```
- Username must match with token
- User must exist
- Contact must be in the contact list

Error Status Code: `400, 401`<br>
Success Status Code: `200`<br>
Status Message: `{"response": "Contact was deleted"}`<br>
Response type: `string`

### HTTPS Server

How to deploy server using https?

- Generate a self-signed certificate by running the following in your shell:
    ```bash
    mkdir ssl
    cd ssl
    openssl genrsa -out key.pem
    openssl req -new -key key.pem -out csr.pem
    openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem
    rm csr.pem
    ```

- Update config.json file (set `config.server.https` and `config.client.https` to `true`)

For more information visit [nodejs.org](https://nodejs.org/en/knowledge/HTTP/servers/how-to-create-a-HTTPS-server/)

## Files

Flie structure of the project

```bash
tree -I 'node_modules|test'
```

```bash
.
├── database
│   ├── users.db
│   └── users.sql
├── LICENSE
├── public
│   ├── 404.html
│   ├── 503.html
│   ├── app
│   │   └── index.html
│   ├── assets
│   │   ├── color.palette
│   │   ├── logo.png
│   │   └── logo.svg
│   ├── blank.html
│   ├── config.js
│   ├── css
│   │   ├── app.css
│   │   ├── default.css
│   │   └── form.css
│   ├── favicon
│   │   ├── android-chrome-192x192.png
│   │   ├── android-chrome-512x512.png
│   │   ├── apple-touch-icon.png
│   │   ├── favicon-16x16.png
│   │   └── favicon-32x32.png
│   ├── favicon.ico
│   ├── index.html
│   ├── js
│   │   ├── app.js
│   │   ├── login.js
│   │   ├── server.js
│   │   └── signup.js
│   ├── libs
│   │   └── DH.min.js
│   ├── login.html
│   ├── noscript.html
│   ├── package.json
│   ├── package-lock.json
│   ├── signup.html
│   └── site.webmanifest
├── README.md
├── server
│   ├── app.js
│   ├── config.json
│   ├── libs
│   │   └── DH.js
│   ├── package.json
│   ├── package-lock.json
│   ├── router
│   │   └── router.js
│   └── services
│       ├── database.js
│       ├── server.js
│       └── socket.js
└── ssl
    ├── cert.pem
    └── key.pem

13 directories, 44 files
```

## License

MIT License

Copyright (c) 2022 Safe Chat

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
