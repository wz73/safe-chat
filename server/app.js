console.log("Launching Safe Chat Server...");

const config = require("./config.json");

const database = require("./services/database")(config);
if (database.open()) {
    throw `Error opening database connection at ${config.database.path}`;
}

const server = require("./services/server")(config, database.get());
server.listen();

const socket = require("./services/socket")(config, server.get(), database.get());
socket.listen();

function exitHandler(options) {
    if (options.exit) {
        socket.close();
        server.close();
        database.close();
        process.exit();
    }
}

process.stdin.resume();
process.on("exit", exitHandler.bind(null, { exit: false }));
process.on("SIGINT", exitHandler.bind(null, { exit:true }));
process.on("SIGUSR1", exitHandler.bind(null, { exit:true }));
process.on("SIGUSR2", exitHandler.bind(null, { exit:true }));
process.on("uncaughtException", (e) => {
    console.error(e);
    process.exit(1);
});