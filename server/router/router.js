const router = {

    routes: {},

    all: (route, callback) => {
        router.routes[route] = {method: 0, callback: callback};
    },

    get: (route, callback) => {
        router.routes[route] = {method: "GET", callback: callback};
    },
    
    put: (route, callback) => {
        router.routes[route] = {method: "PUT", callback: callback};
    },
    
    post: (route, callback) => {
        router.routes[route] = {method: "POST", callback: callback};
    },
    
    delete: (route, callback) => {
        router.routes[route] = {method: "DELETE", callback: callback};
    },
    
    error: {

        route: (req, res) => {
            res.writeHead(404, {"Content-Type": "application/json"});
            res.write(`{"response": "404 Not Found"}`);
            res.end();
        },

        method: (req, res) => {
            res.writeHead(405, {"Content-Type": "application/json"});
            res.write(`{"response": "Method Not Allowed"}`);
            res.end();
        }

    },
    
    route: () => {
        return (request, response) => {

            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Request-Method", "*");
            response.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
            response.setHeader("Access-Control-Allow-Headers", "*");

            var path = request.url.split("?").shift();
            
            try {

                var route = router.routes[path];

                if (route.method && route.method != request.method) {
                    router.error.method(request, response);
                } else {

                    let buffer = new String();

                    request.on("data", (chunk) => {
                        if (buffer.length <= 256) {
                            buffer += chunk;
                        }
                    });

                    request.on("end", () => {
                        route.callback(request, response, buffer);
                    });
                    
                }
                
            } catch (e) {
                router.error.route(request, response);
            }
            
        }
    }

};

module.exports = router;