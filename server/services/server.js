const server = (config, db) => {

    const router = require("../router/router.js");
    const crypto = require("crypto");
    const bcrypt = require("bcrypt");

    let server = null;
    
    if (config.server.https) {

        const https = require("https");
        const fs = require("fs");
        const options = {
            key: fs.readFileSync(config.server.ssl.key),
            cert: fs.readFileSync(config.server.ssl.cert)
        };
        server = https.createServer(options, router.route());

    } else {

        const http = require("http");
        server = http.createServer(router.route());

    }

    router.all("/api", (req, res, buffer) => {
        res.writeHead(200, {"Content-Type": "application/json"});
        res.write(`{"response": "Api working"}`);
        res.end();
    });

    router.post("/api/user/login", async (req, res, buffer) => {

        let code = 400;
        let message = `"Values are missing or invalid"`;

        try {

            const data = JSON.parse(buffer);
            const username = data.username;
            const password = data.password;
            const rows = db.prepare("SELECT * FROM users WHERE name = ?").all(username);

            if (rows.length && await bcrypt.compare(password, rows[0].hash)) {

                code = 200;
                message = `"${rows[0].token}"`;

            } else if (username && password) {

                code = 401;
                message = `"User and password doesn't match"`;

            }

        } catch (e) {}

        res.writeHead(code, {"Content-Type": "application/json"});
        res.write(`{"response": ${message}}`);
        res.end();

    });

    router.post("/api/user/create", async (req, res, buffer) => {

        let code = 400;
        let message = `"Values are missing or invalid"`;

        try {

            const data = JSON.parse(buffer);
            const username = data.username;
            const password = data.password;

            if (
                username.length >= 3 &&
                username.length <= 20 &&
                password.length >= 8 &&
                /[A-Z]/.test(password) &&
                /[a-z]/.test(password) &&
                /[0-9]/.test(password)
            ) {

                const token = crypto.randomBytes(32).toString("hex");
                const hash = await bcrypt.hash(password, config.bcrypt.saltRounds);
                const rows = db.prepare("SELECT id FROM users WHERE name = ?").all(username);

                if (!rows.length) {

                    db.prepare("INSERT INTO users (name, hash, token) VALUES (?, ?, ?)").run(username, hash, token);
                    code = 200;
                    message = `"${token}"`;

                } else {

                    code = 400;
                    message = `"Username is already taken"`;
                
                }

            }

        } catch (e) {}

        res.writeHead(code, {"Content-Type": "application/json"});
        res.write(`{"response": ${message}}`);
        res.end();

    });

    router.post("/api/user/delete", async (req, res, buffer) => {

        let code = 400;
        let message = `"Values are missing or invalid"`;

        try {

            const data = JSON.parse(buffer);
            const username = data.username;
            const token = data.token;
            const rows = db.prepare("SELECT * FROM users WHERE name = ?").all(username);

            if (rows.length && token == rows[0].token) {

                db.prepare("DELETE FROM users WHERE name = ?").run(username);
                code = 200;
                message = `"User deleted successfully"`;

            } else if (username && token) {

                code = 401;
                message = `"Username and token doesn't match"`;

            }

        } catch (e) {}

        res.writeHead(code, {"Content-Type": "application/json"});
        res.write(`{"response": ${message}}`);
        res.end();

    });

    router.post("/api/user/contacts", async (req, res, buffer) => {

        let code = 400;
        let message = `"Values are missing or invalid"`;

        try {

            const data = JSON.parse(buffer);
            const username = data.username;
            const token = data.token;
            const rows = db.prepare("SELECT * FROM users WHERE name = ?").all(username);
            const CONTACTS = new Array();

            if (rows.length && token == rows[0].token) {

                const contacts = JSON.parse(rows[0].contacts);

                for (let i = 0; i < contacts.length; i++) {

                    const contact_rows = db.prepare("SELECT * FROM users WHERE id = ?").all(contacts[i]);
                    if (contact_rows.length) CONTACTS.push(contact_rows[0].name);

                }

                code = 200;
                message = `${JSON.stringify(CONTACTS)}`;

            } else if (username && token) {

                code = 401;
                message = `"Username and token doesn't match"`;

            }

        } catch (e) {
            console.log(e);
        }

        res.writeHead(code, {"Content-Type": "application/json"});
        res.write(`{"response": ${message}}`);
        res.end();

    });

    router.post("/api/user/contacts/create", async (req, res, buffer) => {

        let code = 400;
        let message = `"Values are missing or invalid"`;

        try {

            const data = JSON.parse(buffer);
            const username = data.username;
            const token = data.token;
            const contact = data.contact;
            const rows = db.prepare("SELECT * FROM users WHERE name = ?").all(username);

            if (username == contact) {

                code = 400;
                message = `"Can't add yourself into contacts"`;

            } else if (rows.length && token == rows[0].token) {

                const contacts = JSON.parse(rows[0].contacts);
                const contact_rows = db.prepare("SELECT * FROM users WHERE name = ?").all(contact);

                if (contact_rows.length) {

                    const contact_id = contact_rows[0].id;

                    if (!contacts.includes(contact_id)) {

                        contacts.push(contact_id);
                        db.prepare("UPDATE users SET contacts = ? WHERE name = ?").run(
                            JSON.stringify(contacts),
                            username
                        );
                        code = 200;
                        message = `"Contact was created"`;
    
                    } else {
    
                        code = 400;
                        message = `"User is already in the contact list"`;
    
                    }

                } else {

                    code = 400;
                    message = `"User doesn't exist"`;

                }

            } else if (username && token) {

                code = 401;
                message = `"Username and token doesn't match"`;

            }

        } catch (e) {}

        res.writeHead(code, {"Content-Type": "application/json"});
        res.write(`{"response": ${message}}`);
        res.end();

    });

    router.post("/api/user/contacts/delete", async (req, res, buffer) => {

        let code = 400;
        let message = `"Values are missing or invalid"`;

        try {

            const data = JSON.parse(buffer);
            const username = data.username;
            const token = data.token;
            const contact = data.contact;
            const rows = db.prepare("SELECT * FROM users WHERE name = ?").all(username);

            if (rows.length && token == rows[0].token) {

                const contacts = JSON.parse(rows[0].contacts);
                const contact_rows = db.prepare("SELECT * FROM users WHERE name = ?").all(contact);

                if (contact_rows.length) {

                    const contact_id = contact_rows[0].id;
                    const index = contacts.indexOf(contact_id);

                    if (index !== -1) {

                        contacts.splice(index, 1);
                        db.prepare("UPDATE users SET contacts = ? WHERE name = ?").run(
                            JSON.stringify(contacts),
                            username
                        );
                        code = 200;
                        message = `"Contact was deleted"`;

                    } else {

                        code = 400;
                        message = `"User is not in the contact list"`;

                    }

                } else {

                    code = 400;
                    message = `"User doesn't exist"`;

                }

            } else if (username && token) {

                code = 401;
                message = `"Username and token doesn't match"`;

            }

        } catch (e) {}

        res.writeHead(code, {"Content-Type": "application/json"});
        res.write(`{"response": ${message}}`);
        res.end();

    });

    return {

        get: () => {
            return server;
        },

        listen: () => {

            console.log(`Listening server at http${(config.server.https ? "s" : "")}://${config.server.address}:${config.server.port}`);
            server.listen(config.server.port, config.server.address);

            server.on("error", (e) => {

                if (e.code === "EADDRINUSE") {
                    
                    console.log("Address in use, retrying...");
                    setTimeout(() => {
                        server.close();
                        server.listen(config.server.port, config.server.address);
                    }, 1000);

                }

            });

        },
    
        close: () => {

            console.log(`Clossing server at http${(config.server.https ? "s" : "")}://${config.server.address}:${config.server.port}`);
            server.close();

        }

    }

}

module.exports = server;