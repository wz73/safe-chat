const database = (config) => {

    const sqlite = require("better-sqlite3");
    let database = null;

    return {

        get: () => {
            return database;
        },

        open: () => {

            console.log(`Openning sqlite database connection at ${config.database.path}`);
            database = new sqlite(config.database.path);
            return database.open ? 0 : 1;

        },

        close: () => {

            console.log(`Clossing sqlite database connection at ${config.database.path}`);
            database.close();

        },

    }

}

module.exports = database;