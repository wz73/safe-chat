const socket = (config, server, db) => {

    const DiffieH = require("../libs/DH.min.js")({
        baseBits: 32,
        modulusBits: 4000
    });

    let io = null;
    let listen = null;

    const options =  {
        cors: {
	    origin: "http" + (config.client.https ? "s" : "") + "://" + config.client.address + (config.client.port ? ":" + config.client.port : "")
        }
    }

    const BASE = DiffieH.genBase();
    const MODULUS = DiffieH.genModulus();

    const socketListen = () => {

        let users = new Object();

        listen = io.on("connection", (socket) => {

            socket.on("createUser", (uname, utoken) => {

                if (socket.uname) return;

                const rows = db.prepare("SELECT * FROM users WHERE name = ?").all(uname);

                if (
                    rows.length &&
                    utoken == rows[0].token &&
                    !users[uname]
                ) { 
                    users[uname] = {
                        id: socket.id,
                        sharedKey: null,
                        conversations: new Array()
                    }
                    socket.uname = uname;
                    socket.emit("publicKeys", {
                        BASE: BASE,
                        MODULUS: MODULUS
                    });
                } else {
                    socket.emit("instanceErr");
                }

            });

            socket.on("sharedKey", (shared) => {

                if (!socket.uname) return;
                users[socket.uname].sharedKey = shared;

            });

            socket.on("exchangeKeys", (username) => {

                if (!socket.uname) return;

                if (!users[username]) {
                    socket.emit("receiveShared", null);
                    return;
                }

                users[socket.uname].conversations.push(username);
                users[username].conversations.push(socket.uname);

                socket.emit("receiveShared", {
                    username: username,
                    sharedKey: users[username].sharedKey
                });

                io.to(users[username].id).emit("receiveShared", {
                    username: socket.uname,
                    sharedKey: users[socket.uname].sharedKey
                });

            });

            socket.on("sendMessage", (username, message) => {

                if (!socket.uname || !users[username]) return;
                io.to(users[username].id).emit("receiveMessage", socket.uname, message);
                
            });

            socket.on("closeConversation", (username) => {

                if (!socket.uname || !users[username]) return;

                const index = users[socket.uname].conversations.indexOf(username);

                if (index !== -1) {
                    users[socket.uname].conversations.splice(index, 1);
                    io.to(users[username].id).emit("userLeft", socket.uname);
                }

            });

            socket.on("disconnect", () => {

                if (!socket.uname) return;

                if (Object.hasOwn(users[socket.uname], "conversations")) {

                    for (user of users[socket.uname].conversations) {

                        io.to(users[user].id).emit("userLeft", socket.uname);

                        const index = users[user].conversations.indexOf(socket.uname);
                        if (index !== -1) users[user].conversations.splice(index, 1);

                    }

                }

                delete users[socket.uname];

            });

        });

   }

    return {

        listen: () => {

            console.log(`Listening socket at http${(config.server.https ? "s" : "")}://${config.server.address}:${config.server.port}`);
            io = require("socket.io")(server, options);
            if (listen == null) socketListen();

        },

        close: () => {

            console.log(`Clossing socket at http${(config.server.https ? "s" : "")}://${config.server.address}:${config.server.port}`);
            io.close();

        },

    }

}

module.exports = socket;
