CREATE TABLE users (
    id integer PRIMARY KEY AUTOINCREMENT,
    name varchar(256) NOT NULL,
    hash varchar(256) NOT NULL,
    token varchar(256) NOT NULL,
    contacts TEXT DEFAULT "[]"
);