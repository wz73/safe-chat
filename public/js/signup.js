const utoken = localStorage.getItem("sc-utoken");
const uname = localStorage.getItem("sc-uname");

if (utoken != undefined && uname != undefined) {
    redirect("./app");
}

(async () => {

    try {
        await server.api();
    } catch (e) {
        redirect("../503.html");
    }

})();

const username = document.getElementById("username");
const password = document.getElementById("password");
const confirm = document.getElementById("confirm");
const submit = document.getElementById("submit");

submit.onclick = async () => {

    if (!username.value) {
        return alert("Please enter your username");
    }

    if (
        username.value.length < 3 ||
        username.value.length > 20
    ) {
        return alert("Username must have a length between 2-20 characters");
    }

    if (!password.value) {
        return alert("Please enter a password");
    }

    if (password.value.length < 8) {
        return alert("Password must have a minimum length of 8 characters");
    }

    if (!(
        /[A-Z]/.test(password.value) &&
        /[a-z]/.test(password.value) &&
        /[0-9]/.test(password.value)
    )) {
        return alert("Password must contain lowercases, uppercases, and digits");
    }

    if (password.value != confirm.value) {
        return alert("Password doesn't match with confirm");
    }

    try {

        let request = await server.user.create(username.value, password.value);
        let response = JSON.parse(request.response);

        if (request.status != 200) {
            return alert(response.response);
        }

        localStorage.setItem("sc-utoken", response.response);
        localStorage.setItem("sc-uname", username.value);

    } catch (e) {
        redirect("./503.html");
    }

    redirect("./app");

}

function redirect(href) {
    const a = document.createElement("a");
    a.href = href;
    a.click();
}