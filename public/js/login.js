const utoken = localStorage.getItem("sc-utoken");
const uname = localStorage.getItem("sc-uname");

if (utoken != undefined && uname != undefined) {
    redirect("./app");
}

(async () => {

    try {
        await server.api();
    } catch (e) {
        redirect("../503.html");
    }

})();

const username = document.getElementById("username");
const password = document.getElementById("password");
const submit = document.getElementById("submit");

submit.onclick = async () => {

    if (!username.value) {
        return alert("Please enter your username");
    }

    if (!password.value) {
        return alert("Please enter your password");
    }

    try {

        let request = await server.user.login(username.value, password.value);
        let response = JSON.parse(request.response);

        if (request.status != 200) {
            return alert(response.response);
        }

        localStorage.setItem("sc-utoken", response.response);
        localStorage.setItem("sc-uname", username.value);

    } catch (e) {
        redirect("./503.html");
    }

    redirect("./app");

}

function redirect(href) {
    const a = document.createElement("a");
    a.href = href;
    a.click();
}