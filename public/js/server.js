const asyncXmlHttpRequest = (url, method, body) => {

    return new Promise(function (resolve, reject) {

        var xhr = new XMLHttpRequest();
        xhr.open(method, url, true);

        xhr.onload = () => {
            resolve(xhr);
        };

        xhr.onerror = () => {
            reject(xhr);
        }
        
        xhr.send(body);

    });

}

const apiURL = "http" + (config.server.https ? "s" : "") + "://" + config.server.address + ":" + config.server.port + "/api";

const server = {

    api: () => {
        return asyncXmlHttpRequest(apiURL, "GET", null);
    },

    user: {

        login: (username, password) => {

            return asyncXmlHttpRequest(apiURL + "/user/login", "POST", JSON.stringify({
                username: username,
                password: password
            }));

        },

        validate: (username, token) => {

            return asyncXmlHttpRequest(apiURL + "/user/validate", "POST", JSON.stringify({
                username: username,
                token: token
            }));

        },

        create: (username, password) => {

            return asyncXmlHttpRequest(apiURL + "/user/create", "POST", JSON.stringify({
                username: username,
                password: password
            }));

        },

        delete: (username, token) => {

            return asyncXmlHttpRequest(apiURL + "/user/delete", "POST", JSON.stringify({
                username: username,
                token: token
            }));

        },

        contacts: {

            fetch: (username, token) => {

                return asyncXmlHttpRequest(apiURL + "/user/contacts", "POST", JSON.stringify({
                    username: username,
                    token: token
                }));

            },

            create: (username, token, contact) => {

                return asyncXmlHttpRequest(apiURL + "/user/contacts/create", "POST", JSON.stringify({
                    username: username,
                    token: token,
                    contact: contact
                }));

            },

            delete: (username, token, contact) => {

                return asyncXmlHttpRequest(apiURL + "/user/contacts/delete", "POST", JSON.stringify({
                    username: username,
                    token: token,
                    contact: contact
                }));
                
            }

        }

    }

}