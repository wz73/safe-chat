(async () => {

/**
 * Gets user token and username
 */
utoken = localStorage.getItem("sc-utoken");
uname = localStorage.getItem("sc-uname");
contacts = new Array();

// Save messages
messages = new Object();
previous_username = null;
current_username = null;

/**
 * Request handler for server requests
 * @returns {object}  returns JSON parsed object
 * @param {function} request Server.js built in function
 */
function requestHandler(request) {

    if (!request.response || request.status == 401) {
        console.log(`Server request error (${request.status} - ${request.response})`);
        return logout();
    }

}

/**
 * Redirects user to another url
 * @param {string} href path to redirect
 */
function redirect(href) {
    const a = document.createElement("a");
    a.href = href;
    a.click();
}

/**
 * Redirects user to login page after removing localStorage items
 */
function logout() {
    localStorage.removeItem("sc-utoken");
    localStorage.removeItem("sc-uname");
    redirect("../login.html");
}

/**
 * Encrypts text with AES algorithm
 * @param {*} value 
 * @param {*} password 
 * @returns encrypted text
 */
function encrypt(value, password) {
	return CryptoJS.AES.encrypt(value, password).toString();
}

/**
 * Descrypts text with AES algorithm
 * @param {*} value 
 * @param {*} password 
 * @returns decrypted text
 */
function decrypt(value, password) {
	return CryptoJS.AES.decrypt(value, password).toString(CryptoJS.enc.Utf8);
}

// Check service availability
try {
    await server.api();
} catch (e) { redirect("../503.html") }

// Input message and send button
const input_msg = document.getElementById("input-msg");
const send_msg = document.getElementById("send-msg");

send_msg.onclick = () => {

    if (!send_msg.disabled && input_msg.value) {
        sendMessage();
    }

}

// Graphic user interface functions
GUI = {

    contacts: async () => {

        console.warn("GUI - showing contacts");

        const contacts_req = await server.user.contacts.fetch(uname, utoken);
        requestHandler(contacts_req);
        const contacts_res = JSON.parse(contacts_req.response).response;
        if (typeof contacts_res == "object") contacts = contacts_res;

        const list = document.getElementById("bar-list");
        while (list.firstChild) { list.removeChild(list.lastChild); }

        for (contact of contacts) {

            const item = document.createElement("div");
            item.classList.add("bar-list-item");
            const name = document.createElement("p");
            name.innerText = contact;
            const close = document.createElement("span");
            close.innerText = "x";

            item.onclick = (e) => {
                if (e.target.innerText == "x") return;
                openConversation(name.innerText);
            }

            close.onclick = async () => {

                if (!confirm("Delete contact?")) return;

                item.remove();
                const delete_req = await server.user.contacts.delete(uname, utoken, contact);
                requestHandler(delete_req);

            }

            item.appendChild(name);
            item.appendChild(close);
            list.appendChild(item);

        }

    },

    conversations: () => {

        console.warn("GUI - showing conversations");

        const list = document.getElementById("bar-list");
        while (list.firstChild) { list.removeChild(list.lastChild); }

        for (username in messages) {

            const item = document.createElement("div");
            item.classList.add("bar-list-item");
            const name = document.createElement("p");
            name.innerText = username;
            const close = document.createElement("span");
            close.innerText = "x";

            item.onclick = (e) => {
                if (e.target.innerText == "x") return;
                openConversation(name.innerText);
            }

            close.onclick = async () => {

                if (!confirm("Delete conversation?")) return;

                item.remove();
                delete messages[username];
                closeConversation(name.innerText, true);

            }

            item.appendChild(name);
            item.appendChild(close);
            list.appendChild(item);

        }

    },

    messages: {

        clear: () => {

            console.warn("GUI - clearing messages");

            const messages = document.getElementById("messages");
            while (messages.firstChild) { messages.removeChild(messages.lastChild); }

        },

        append: (username, value, active) => {

            const DATE = new Date;

            const _messages = document.getElementById("messages");

            const message = document.createElement("div");
            message.classList.add("msg");
            if (active) message.classList.add("active");

            const div = document.createElement("div");

            const author = document.createElement("span");
            author.classList.add("msg-author");
            author.innerText = username;

            const date = document.createElement("span");
            date.classList.add("msg-date");
            date.innerText = " - " + [
                DATE.getMonth(),
                DATE.getDate(),
                DATE.getFullYear()
            ].join("/") + " " + [
                DATE.getHours(),
                DATE.getMinutes(),
                DATE.getSeconds()
            ].join(':');

            const txt = document.createElement("div");
            txt.classList.add("msg-text");
            txt.innerText = value;

            div.appendChild(author);
            div.appendChild(date);
            message.appendChild(div);
            message.appendChild(txt);
            _messages.appendChild(message);

            _messages.scrollTo(0, _messages.scrollHeight);

        },

        show: (username) => {

            console.warn("GUI - showing messages");

            // Clear all the messages
            GUI.messages.clear();
            // Append system messages
            GUI.messages.append("System", `This is the start of your conversation between ${username}!`, true);
            // Append messages if exists
            if (messages[username]) {
                for (let i = 0; i < messages[username].length; i++) {
                    GUI.messages.append(
                        messages[username][i].author,
                        messages[username][i].value,
                        messages[username][i].author == uname
                    );
                }
            }

        },

        open: () => {

            console.warn("GUI - message input enabled");
            
            // Enables input and buttons
            input_msg.disabled = false;
            send_msg.disabled = false;
            // Focus input
            input_msg.focus();

        },

        close: () => {

            console.warn("GUI - message input disabled");

            // Disables input and buttons
            input_msg.disabled = true;
            send_msg.disabled = true;

        }

    }

}

/**
 * Returns to login if utoken or username is undefined
 */
if (utoken == undefined || uname == undefined) {
    redirect("../login.html");
}

// Show username
document.getElementById("username").innerText = uname;

// Listen for delete account and logout buttons
const btn_delete = document.getElementById("btn-delete");
const btn_logout = document.getElementById("btn-logout");

btn_logout.onclick = logout;
btn_delete.onclick = async () => {

    if (!confirm("Delete account?")) return;

    const delete_req = await server.user.delete(uname, utoken);
    requestHandler(delete_req);
    logout();

}

// Filter conversations and contacts
const bar_search = document.getElementById("bar-search");
bar_search.oninput = () => {

    document.querySelectorAll(".bar-list-item").forEach((e) => {
        const name = e.querySelector("p").innerText;
        if (name.indexOf(bar_search.value) == -1) {
            e.style.display = "none";
        } else {
            e.style.display = null;
        }
    });

}

// Listen for conversations and contacts buttons
const btn_conversations = document.getElementById("btn-conv");
const btn_contacts = document.getElementById("btn-cont");

btn_conversations.onclick = () => {
    btn_conversations.classList.add("active");
    btn_contacts.classList.remove("active");
    bar_search.value = null;
    GUI.conversations();
};

btn_contacts.onclick = () => {
    btn_contacts.classList.add("active");
    btn_conversations.classList.remove("active");
    bar_search.value = null;
    GUI.contacts();
};

// Listen for create button
const create = document.getElementById("bar-create");
const create_conversations = document.getElementById("create-conv");
const create_contacts = document.getElementById("create-cont");

create.onclick = () => {

    if (btn_conversations.classList.contains("active")) {

        create_conversations.style.display = null;
        create_contacts.style.display = "none";

    } else {

        create_contacts.style.display = null;
        create_conversations.style.display = "none";

    }

}

// Create conversations and contacts
const conversations_username = document.getElementById("ctv-username");
const conversations_create = document.getElementById("ctv-create");
const conversations_close = document.getElementById("ctv-close");
conversations_close.onclick = conversations_close_fun;

function conversations_close_fun() {

    conversations_username.value = "";
    create_conversations.style.display = "none";

}

conversations_create.onclick = () => {
    
    openConversation(conversations_username.value);
    conversations_close_fun();
    
}

const contacts_username = document.getElementById("ctc-username");
const contacts_create = document.getElementById("ctc-create");
const contacts_close = document.getElementById("ctc-close");
contacts_close.onclick = contacts_close_fun;

function contacts_close_fun() {

    contacts_username.value = "";
    create_contacts.style.display = "none";

}

contacts_create.onclick = async () => {

    const username = contacts_username.value;
    const create_req = await server.user.contacts.create(uname, utoken, username);
    requestHandler(create_req);
    const create_res = JSON.parse(create_req.response).response;

    if (create_req.status == 200){
        contacts_close_fun();
        GUI.contacts();
    }

    alert(create_res);

}

// User private key and shared key
KEYS = {
    DiffieH: null,
    PRIVATE: {
        KEY: null,
        SHARED: null
    },
    PUBLIC: {
        BASE: null,
        MODULUS: null
    },
    SECRETS: new Object()
}

// Setup socket io
socket = io("http" + (config.server.https ? "s" : "") + 
"://" + config.server.address + ":" + config.server.port);

// Create new user once connected
socket.on("connect", () => {
    console.warn("Connecting to socket...");
    socket.emit("createUser", uname, utoken);
});

// Generate public keys and send shared key
socket.on("publicKeys", (keys) => {

    KEYS.PUBLIC.BASE = keys.BASE;
    KEYS.PUBLIC.MODULUS = keys.MODULUS;

    console.warn("Public key \"Base\" received:");
    console.log(KEYS.PUBLIC.BASE);
    console.warn("Public key \"Modulus\" received:");
    console.log(KEYS.PUBLIC.MODULUS.substring(0, 20) + "...");

    KEYS.DiffieH = DH({
        keyBits: 4000,
        baseBits: 32,
        modulusBits: 4000,
        modPow: (a, b, c) => {
            return bigInt(a).modPow(b, c).toString();
        }
    });

    KEYS.DiffieH.base = KEYS.PUBLIC.BASE;
    KEYS.DiffieH.modulus = KEYS.PUBLIC.MODULUS;
    KEYS.DiffieH.genKey();
    KEYS.DiffieH.computeShared();

    console.warn("Private key generated");
    console.log(KEYS.DiffieH.key.substring(0, 20) + "...");
    console.warn("Shared key generated");
    console.log(KEYS.DiffieH.shared.substring(0, 20) + "...");

    socket.emit("sharedKey", KEYS.DiffieH.shared);

});

// Receive user shared key
socket.on("receiveShared", (user) => {

    if (!user) {
        current_username = previous_username;
        return alert("User is not connected!");
    }

    console.warn("Received key from " + user.username);

    KEYS.DiffieH.computeSecret(user.sharedKey);
    KEYS.SECRETS[user.username] = KEYS.DiffieH.secret;

    if (!messages[user.username]) messages[user.username] = new Array();

    if (current_username == user.username) {
        GUI.messages.show(user.username);
        GUI.messages.open();
    }

    btn_conversations.click();
    
});

socket.on("userLeft", (username) => {
    
    if (current_username == username) {
        alert(username + " is no longer connected!");
    }
    
    console.log(username + " is no longer connected!");

    closeConversation(username);

});


socket.on("receiveMessage", (username, message) => {

    console.warn("Received message from " + username);

    const decrypted = decrypt(message, KEYS.SECRETS[username]);
    console.log(decrypted);

    if (current_username == username) {
        GUI.messages.append(username, decrypted);
    }

    messages[username].push({
        author: username,
        value: decrypted
    });
    
});

/**
 * Sends a message through sockets
 */
function sendMessage() {

    const message = input_msg.value;
    const encrypted = encrypt(message, KEYS.SECRETS[current_username]);

    console.warn("Sending new message to " + current_username);
    console.log(message);
    
    socket.emit("sendMessage", current_username, encrypted);
    GUI.messages.append(uname, message, true);
    input_msg.value = null;

    messages[current_username].push({
        author: uname,
        value: message
    });

}

/**
 * Starts a new conversation
 * @param {string} username 
 */
function openConversation(username) {

    console.warn("Opening new conversation with " + username);
    previous_username = current_username;
    current_username = username;
    socket.emit("exchangeKeys", username);

}

/**
 * Closes current conversation
 * @param {string} username 
 */
function closeConversation(username, emit) {

    console.warn("Closing conversation with " + username)

    if (username == current_username) {
        current_username = null;
        GUI.messages.close();
    }

    delete messages[username];
    delete KEYS.SECRETS[username];

    if (emit, socket.emit("closeConversation", username));
    btn_conversations.click();
    
}

socket.on("instanceErr", () => {
    alert("You are already connected from another instance");
    redirect("../blank.html");
});

})();